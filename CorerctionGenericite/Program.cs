﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorerctionGenericite
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Veuillez entrer 2 nombres");
                double n1 = double.Parse(Console.ReadLine());
                double n2 = double.Parse(Console.ReadLine());
                Console.WriteLine(Executer<Quotient>(n1,n2));
            } catch(FormatException e)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(e.Message);
            } catch(ExceptionException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }

        static double Executer<T>(double n1, double n2)
            where T: Operateur, new()
        {
            T instance = new T();
            double result = instance.Operation(n1, n2);
            return result;
        }
    }
}
