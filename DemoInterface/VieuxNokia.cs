﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterface
{
    class VieuxNokia : ITel
    {
        public void Telephoner(string personne)
        {
            Console.WriteLine("Je telephone depuis mon vieux Nokia à " + personne);
        }
    }
}
