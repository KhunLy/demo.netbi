﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterface
{
    class Program
    {
        static void Main(string[] args)
        {

            // 2 telephones dispos
            AiPhone phone = new AiPhone();
            VieuxNokia phone2 = new VieuxNokia();
            SamsungGalaxy phone3 = new SamsungGalaxy();

            // je peux fournir à ma secretaire l'un des 2 telephones
            Secretaire monique = new Secretaire(phone3);
            monique.Nom = "Dupond";
            monique.Prenom = "Monique";
            monique.Commander("Materne");
            Console.ReadKey();

        }
    }
}
