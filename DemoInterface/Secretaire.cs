﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterface
{
    class Secretaire
    {
        private ITel _tel;
        public string Nom { get; set; }
        public string Prenom { get; set; }

        public Secretaire(ITel tel)
        {
            _tel = tel;
        }

        public void Commander(string nomDuFournisseur)
        {
            _tel.Telephoner(nomDuFournisseur);
        }
    }
}
