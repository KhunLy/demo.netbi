﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterface
{
    class AiPhone : ITel
    {
        public string NumeroDeSerie { get; set; }

        public void Telephoner(string personne)
        {
            Console.WriteLine("Je telephone à " + personne);
        }

        public void PrendreDesPhotos()
        {

        }

        public void EnvoyerDesEmails()
        {

        }


    }
}
