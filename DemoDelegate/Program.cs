﻿using CorrectionExoDelegate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDelegate
{

    // declaration d'un delegate
    // delegate TypeDeRetour NomDuDelegate(paramètres)
    delegate bool Test(int v);
    class Program
    {
        static void Main(string[] args)
        {
            //affectation d'une variable de type delegate 
            //Test nomDeMaVariable = TestSiImpair;
            //nomDeMaVariable += TestSiPair;

            //List<int> listeDep = new List<int> { 1,2,3,4,5,6 };
            //Console.WriteLine(string.Join(",", listeDep));

            //// List<int> nouvelleListe = Filter(listeDep, TestSiPair);
            //List<int> nouvelleListe = Filter(listeDep, v => v % 3 == 0);
            ////List<int> nouvelleListe = Filter(listeDep, (int v) => { return v % 3 == 0; });
            //Console.WriteLine(string.Join(",", nouvelleListe));

            List<string> liste = new List<string> { "a", "b", "aa", "ba" };
            List<string> nListe 
                = Filtrer<string>(liste, (string str) => str.StartsWith("a"));
            Console.WriteLine(string.Join(",", nListe));

            Console.ReadKey();
        }

        #region Methodes de tests utilisables dans la fonction "Filter"
        static bool TestSiPair(int v)
        {
            return v % 2 == 0;
        }

        static bool TestSiImpair(int v)
        {
            return v % 2 != 0;
        }
        #endregion

        #region Fonctions de filtres
        static List<int> FiltrerPairs(List<int> listeDepart)
        {
            List<int> resultat = new List<int>();
            foreach (int nb in listeDepart)
            {
                if (TestSiPair(nb))
                {
                    resultat.Add(nb);
                }
            }
            return resultat;
        }

        static List<int> FiltrerImpairs(List<int> listeDepart)
        {
            List<int> resultat = new List<int>();
            foreach (int nb in listeDepart)
            {
                if (TestSiImpair(nb))
                {
                    resultat.Add(nb);
                }
            }
            return resultat;
        }
        #endregion

        /// <summary>
        /// Filtre une liste à l'aide d'une fonction
        /// </summary>
        /// <param name="listeDepart"></param>
        /// <param name="fonction"></param>
        /// <returns>La liste fitrée à l'aide de la fonction</returns>
        static List<int> Filter(List<int> listeDepart, Test fonction)
        {
            List<int> resultat = new List<int>();
            foreach (int nb in listeDepart)
            {
                if (fonction(nb))
                {
                    resultat.Add(nb);
                }
            }
            return resultat;
        }

        static List<T> Filtrer<T>(List<T> listeDeDepart, Func<T, bool> fonction)
            // where T : class, new()
        {
            List<T> resultat = new List<T>();
            foreach (T nb in listeDeDepart)
            {
                if (fonction(nb))
                {
                    resultat.Add(nb);
                }
            }
            return resultat;
        }
    }
}
