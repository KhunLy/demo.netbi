﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDelegate2
{
    delegate void Calcul(float v);
    class Prof
    {
        // événement
        public event Calcul DoubleValeur;
        public string Nom { get; set; }

        #region Encapsulation Complète
        //private string _nom;

        //public string Nom
        //{
        //    get
        //    {
        //        return _nom;
        //    }

        //    set
        //    {
        //        if(value == null)
        //        {
        //            Console.WriteLine("On ne peut pas insérer de valuer null");
        //            return;
        //        }
        //        _nom = value;
        //    }
        //} 
        #endregion

        public void DemanderLeDouble(float v)
        {
            Console.WriteLine(Nom + " : Quel est le double de " + v + "?");
            //éxécuter l'événement
            //if(DoubleValeur != null)
            //    DoubleValeur(v);
            DoubleValeur?.Invoke(v);
        }
    }
}
