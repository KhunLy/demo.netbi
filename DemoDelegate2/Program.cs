﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDelegate2
{
    class Program
    {
        static void Main(string[] args)
        {
            Prof p = new Prof();
            p.Nom = "Khun";
            Prof p2 = new Prof();
            p2.Nom = "Guillaume";


            Etudiant e1 = new Etudiant();
            e1.Prenom = "Jerome";

            Etudiant e2 = new Etudiant();
            e2.Prenom = "Pascal";

            Etudiant e3 = new Etudiant();
            e3.Prenom = "Yussuf";

            e1.Ecouter(p);
            // e1.Ecouter(p2);

            e3.Ecouter(p);

            p2.DemanderLeDouble(5.42f);
            // declencher l'événement est encapsulé dans la méthode Demander le double
            // p2.DoubleValeur(5.42f);

            Console.ReadKey();
        }
    }
}
