﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDelegate2
{
    class Etudiant
    {
        public string Prenom { get; set; }

        public void Ecouter(Prof p)
        {
            // abonnement à l'événement
            p.DoubleValeur += CalculDouble;
        }

        private void CalculDouble(float v)
        {
            Console.WriteLine($"{Prenom} repond {v*2}");
        }
    }
}
