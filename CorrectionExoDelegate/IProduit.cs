﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionExoDelegate
{
    interface IProduit
    {
        double Prix { get; set; }
        string Nom { get; set; }
        void AugmenterSonPrix();
    }
}
