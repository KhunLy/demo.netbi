﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionExoDelegate
{
    class Magasin
    {
        private List<IProduit> _stock;

        // public string Nom { get; set; }

        //private event Augmentation _augmentationDesPrix;
        private event Action _augmentationDesPrix;

        public Magasin()
        {
            _stock = new List<IProduit>();
        }

        //public Magasin(string nom): this()
        //{
        //    Nom = nom;
        //}

        public void Ajouter(IProduit p)
        {
            _stock.Add(p);
            // abonnement à l'événement
            _augmentationDesPrix += p.AugmenterSonPrix; 
        }

        public void AfficherLesProduits()
        {
            foreach(IProduit p in _stock)
            {
                Console.WriteLine($"{p.Nom} : {p.Prix}");
            }
        }

        public void AugmenterTousLesPrix()
        {
            Console.WriteLine("Tous les prix vont être augmentés");
            // déclencher l'aumentation des prix
            _augmentationDesPrix?.Invoke();
        }


    }
}
