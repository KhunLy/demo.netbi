﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionExoDelegate
{
    class Cigarette : IProduit
    {
        public double Prix { get; set; }
        public string Nom { get; set; }
        public void AugmenterSonPrix()
        {
            Prix += (Prix * 5) / 100;
        }
    }
}
