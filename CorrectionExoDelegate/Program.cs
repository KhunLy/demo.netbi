﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionExoDelegate
{
    class Program
    { 
        static void Main(string[] args)
        {
            Magasin m = new Magasin();
            //IProduit p = new Alcool();
            //p.Nom = "Willam Lawson";
            //p.Prix = 12;
            IProduit p = new Alcool
            {
                Nom = "Willam Lawson",
                Prix = 12
            };
            IProduit p1 = new Alcool
            {
                Nom = "Havana Club",
                Prix = 13.5
            };
            IProduit p2 = new Cigarette
            {
                Nom = "Marlboro",
                Prix = 6
            };
            m.Ajouter(p);
            m.Ajouter(p1);
            m.Ajouter(p2);
            Console.WriteLine("Etat du magasin avant l'augmentation");
            m.AfficherLesProduits();

            m.AugmenterTousLesPrix();

            Console.WriteLine("Etat du magasin après l'augmentation");
            m.AfficherLesProduits();
            Console.ReadKey();

            // Func<int> maFonction;



        }
    }
}
